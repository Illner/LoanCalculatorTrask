import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import { ApiResponseError, ApiResponse } from './api-model';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

// TN means it is type named by property name
// TP means type named by property path - can be same like named by name, so diversed

/**
 * Trask DevOps academy demo seed application
 */
@Injectable({
  providedIn: 'root'
})
export class GatewayService {

  constructor(public http: HttpClient) {}

  /**
   * TODO  '/api/' přepsat na '/api/<název endpointu>/
   */
  public getInterestRate(): Observable<ApiResponse> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    }
      );

    let url = `${environment.apiBase}/getInterestRate`;

    return this.http.get(url,
      {
        responseType: 'json',
        headers: headers}) as any;
  }
}

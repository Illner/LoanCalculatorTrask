# Trask - hypoteční kalkulačka

## Důležité odkazy
[GitLab repozitář kalkulačky](https://gitlab.com/Illner/LoanCalculatorTrask/)

[GitLab repozitář dokumentace v hugo](https://gitlab.com/addrid/trask-academy-hugodoc)

[Online adresář (vytvořte)](TBA)

[Nástroje (git, node.js, webstorm)](https://1drv.ms/u/s!AhmKk4F2XxrCiFi9pzFDoDRundeS?e=GM56Ts)

[Online dokumentace v hugo](https://addrid.gitlab.io/trask-academy-hugodoc/)

## Možná podoba výstupu pro inspiraci
![Možná podoba hypoteční kalkulačky](public/images/trask_kalkulacka_prototyp.png "Trask kalkulačka")

## Pokyny pro zprovoznění (lokálně)
_Předpokládá se, že máš nainstalovaný git, nodejs a npm._
* spusť příkazovou řádku a přejdi do složky, ve které chceš mít projekt (lze přes Webstorm terminál)
```
cd <absolutní cesta ke složce>
```
* vytvoř libovolný adresář
```
mkdir <název adresáře>
```
* přejdi do vytvořeného adresáře
```
cd <název adresáře>
```
* do vytvořené složky naklonuj gitlab repozitář
```
git clone git@gitlab.com:Illner/LoanCalculatorTrask.git
```
* přejdi do adresáře s naklonovaným projektem
```
cd LoanCalculatorTrask
```
* stáhni a nainstaluj všechny závislosti
```
npm install
```
* po dokončení již můžeš spustit server
```
npm start
```
* klient potom běží na ```http:localhost:4200/```


## Pokyny pro vývoj
* forknout si [projekt](https://gitlab.com/Illner/LoanCalculatorTrask)
* pro správné fungování GitLabu je potřeba vytvořit [SSH klíče](https://docs.gitlab.com/ee/ssh/README.html)
* implementovat [GitLab pages](https://medium.com/@atiaxi/publishing-a-standalone-angular-app-on-gitlab-pages-b58458d2c94)
